//
//  AccountVC.swift
//  Fresh
//
//  Created by Mario Gamal on 5/11/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func CallBtn(_ sender: Any) {
        showCallDialog()
    }
    
    func showCallDialog(){
        let actionSheetController: UIAlertController = UIAlertController(title: "الخط الساخن", message: "٠١٢١١١٨٤٥٤", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "الغاء", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        
        let callAction: UIAlertAction = UIAlertAction(title: "اتصال", style: .default) { action -> Void in
            if let url = URL(string: "tel://01211118454"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        actionSheetController.addAction(callAction)
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    @IBAction func SignUp(_ sender: Any) {
        
        let userdefault = UserDefaults.standard
        userdefault.set(nil, forKey: "user_id")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
