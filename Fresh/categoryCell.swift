//
//  categoryCell.swift
//  Fresh
//
//  Created by Monica Girgis on 5/5/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class categoryCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
}
