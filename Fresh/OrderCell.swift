//
//  OrderCell.swift
//  Fresh
//
//  Created by Mario Gamal on 5/11/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    
    @IBOutlet weak var orderState: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var arriveTime: UILabel!
    @IBOutlet weak var proName: UILabel!
    @IBOutlet weak var proQty: UILabel!
    @IBOutlet weak var proPrice: UILabel!
    @IBOutlet weak var totalMoney: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
