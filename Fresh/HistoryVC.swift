//
//  HistoryVC.swift
//  Fresh
//
//  Created by Mario Gamal on 5/26/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire

class HistoryVC: UIViewController , UITableViewDataSource , UITableViewDelegate {
    
    var orders:[Order] = []
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return orders.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTable.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        cell.selectionStyle = .none
        cell.orderDate.text = orders[indexPath.section].orderDate
        cell.arriveTime.text = orders[indexPath.section].arrivalTime
        
        let jsonArray = orders[indexPath.section].orderItems
        var itemNames = ""
        var itemsQty = ""
        var itemsPrice = ""
        var total = 0.0

        for jsonObject in jsonArray! {
            guard
                let name = jsonObject["name"] as? String,
                let price = jsonObject["price"] as? String,
                let qty = jsonObject["qty"] as? Int
                else {
                    
                    print("error")
                    continue}
            total += Double(qty)*Double(price)!
            itemNames += name+"\n"
            itemsQty += "\(qty)"+"\n"
            itemsPrice += price+"\n"
        }
        if (orders[indexPath.section].orderDisscount=="0"){
            cell.totalMoney.text = String(total)
        }
        else {
            cell.totalMoney.text = String((total*90)/100)
        }
        
        cell.proName.text = itemNames
        cell.proPrice.text = itemsPrice
        cell.proQty.text = itemsQty
        
        cell.proName.sizeToFit()
        cell.proPrice.sizeToFit()
        cell.proQty.sizeToFit()
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        return cell
    }
    
    
    @IBOutlet weak var historyTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTable.delegate = self
        historyTable.dataSource = self
        historyTable.estimatedRowHeight = 150
        historyTable.rowHeight = UITableViewAutomaticDimension
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let userdefault = UserDefaults.standard
        
        let parameters : Parameters = [
            "user_id":"\(userdefault.string(forKey: "user_id")!)",
            "confirmation_status":"2"]
        
        Alamofire.request("\(MainVC.url)track_user_orders.php",method: .post,parameters: parameters).responseJSON { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                if let jsonArray = response.result.value as? [[String: Any]] {
                    print(jsonArray)
                    for jsonObject in jsonArray
                    {
                        guard
                            let id = jsonObject["id"] as? String,
                            let state = jsonObject["confirmation_status"] as? String,
                            let items = jsonObject["order_items"] as? String,
                            let date = jsonObject["created_on"] as? String,
                            let arrive = jsonObject["delivery_time"] as? String,
                            let disscount = jsonObject["has_discount"] as? String
                            else {
                                print("fail")
                                continue
                        }
                        let data = items.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                        do {
                            let jsonItems = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: AnyObject]]
                            self.orders.append(Order(id:id, items:jsonItems, date:date, arrive:arrive, state:state, disscount:disscount))
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                    self.historyTable.reloadData()
                }
                else
                {
                    print("fail")
                }
            }
        }
    }
}
