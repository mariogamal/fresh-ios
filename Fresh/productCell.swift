//
//  productCell.swift
//  Fresh
//
//  Created by Monica Girgis on 5/5/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class productCell: UITableViewCell {
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var proName: UILabel!
    @IBOutlet weak var proQty: UILabel!
    @IBOutlet weak var proPrice: UILabel!
    @IBOutlet weak var buyProduct: UIButton!
    @IBOutlet weak var increaseQty: UIButton!
    @IBOutlet weak var decreaseQty: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
