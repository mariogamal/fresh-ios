//
//  InfoEdit.swift
//  Fresh
//
//  Created by Mario Gamal on 5/25/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire
class InfoEdit: UIViewController , UITextFieldDelegate , UITextViewDelegate ,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var special: UITextView!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var areapicker: UIPickerView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var secview: UIView!
    @IBOutlet weak var password: UITextField!
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var citiesName = [String]()
    var citiesID = [String]()
    var citySelectedIndex : Int = 0
    
    @IBAction func editInfo(_ sender: Any) {
        updateData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        address.delegate = self
        email.delegate = self
        special.delegate = self
        mobile.delegate = self
        
        address.text = "    العنوان بالتفصيل    "
        address.textColor = UIColor.lightGray
        
        special.text = "     اقرب علامة مميزة    "
        special.textColor = UIColor.lightGray
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.secview.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(tabontext(gesture:)))
        tapGesture2.numberOfTapsRequired = 1
        self.password.addGestureRecognizer(tapGesture2)
        
        loadCities()
        
    }
    
    func loadUserDate(){
        
        let parameters : Parameters = [
            "user_id":"\(UserDefaults.standard.string(forKey: "user_id")!)"]
        
        Alamofire.request("\(MainVC.url)user_data.php",method: .post,parameters: parameters).responseJSON { response in
            self.activityIndicator.stopAnimating()
            
            if(response.result.isSuccess)
            {
                let jsonArray = response.result.value as? [[String: Any]]
                print(jsonArray!)
                let username = jsonArray![0]["username"] as? String
                let address = jsonArray![0]["address"] as? String
                let email = jsonArray![0]["email"] as? String
                let phone = jsonArray![0]["phone"] as? String
                let special_location = jsonArray![0]["special_location"] as? String
                let city_name = jsonArray![0]["city_name"] as? String
                self.name.text = username
                self.address.text = address
                self.email.text = email
                self.mobile.text = phone
                self.special.text = special_location
                let cityIndex = self.getCityIndex(cityName: city_name!)
                self.areapicker.selectRow(cityIndex, inComponent: 0, animated: false)
            }
        }
    }
    
    func getCityIndex(cityName : String)-> Int{
        
        var index = 0
        for city in citiesName
        {
            if city == cityName
            {
                return index
            }
            index = index + 1
        }
        return 0
    }
    
    func loadCities(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        Alamofire.request("\(MainVC.url)cities.php").responseJSON{ response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                if let array = response.result.value as? [[String: Any]] {
                    self.citiesName = array.flatMap { $0["name"] as? String }
                    self.citiesID = array.flatMap { $0["id"] as? String }
                    self.areapicker.reloadAllComponents()
                    print(self.citiesName)
                    self.loadUserDate()
                }
                else
                {
                    print("fail")
                }
            }
        }
        
        // Do any additional setup after loading the view.
        self.areapicker.dataSource = self
        self.areapicker.delegate = self
    }
    
    // Mina Add Tapgesture to dismiss keyboard when tap on view
    @IBAction func tapGestureHandler(gesture: UITapGestureRecognizer)
    {
        
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        scrollview.setContentOffset(CGPoint.init(x: 0, y: 400), animated: true)
        
        if address.textColor == UIColor.lightGray && textView == address{
            address.text = nil
            
            address.textColor = UIColor.black
        }
        
        if special.textColor == UIColor.lightGray && textView == special {
            special.text = nil
            special.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        scrollview.setContentOffset(CGPoint.init(x: 0, y: scrollview.contentOffset.y - 100), animated: true)
        
        if address.text.isEmpty {
            address.text = "    العنوان بالتفصيل    "
            address.textColor = UIColor.lightGray
        }
        
        if special.text.isEmpty {
            special.text = "     اقرب علامة مميزة    "
            special.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        name.resignFirstResponder()
        email.resignFirstResponder()
        mobile.resignFirstResponder()
        return true
    }
    
    func updateData(){
        let parameters : Parameters = [
            "user_id":"\(UserDefaults.standard.string(forKey: "user_id")!)",
            "username":name.text!,
            "phone":mobile.text!,
            "email":email.text!,
            "address":address.text!,
            "special_location":special.text!,
            "city_id":citiesID[citySelectedIndex]]
        
        Alamofire.request("\(MainVC.url)update_user.php",method: .post,parameters: parameters).responseString { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                if ((resultString?.range(of: "true")) != nil)
                {
                    let alert = UIAlertController(title: "تعديل البيانات", message: "تم التعديل بنجاح", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: { action in
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                        self.present(newViewController, animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else
                {
                    let alert = UIAlertController(title: "خطأ", message: "برجاء اعادة المحاولة", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citiesName.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return citiesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        citySelectedIndex = row
    }
    
    @IBAction func tabontext(gesture: UITapGestureRecognizer)
    {
        
        let alert = UIAlertController(title: "تغيير كلمة المرور",
                                      message: "",
                                      preferredStyle: .alert)
        
        let confirmaction = UIAlertAction(title: "تغيير",style: .default) { (_) in
            
            guard let textFields = alert.textFields,
                textFields.count > 0 else {
                    return
            }
            
            let userdefault = UserDefaults.standard
            let parameters : Parameters = [
                "user_id":"\(userdefault.string(forKey: "user_id")!)",
                "old":textFields[0].text!,
                "new":textFields[1].text!]
            
            Alamofire.request("\(MainVC.url)change_password.php",method: .post,parameters: parameters).responseString { response in
                self.activityIndicator.stopAnimating()
                if(response.result.isSuccess)
                {
                    let resultString = response.result.value
                    if ((resultString?.range(of: "true")) != nil)
                    {
                        let alert = UIAlertController(title: "تغيير كلمة المرور", message: "تم التغيير بنجاح", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: {  action in
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                            self.present(newViewController, animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "خطأ", message: "كلمة المرور خاطئة", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "الغاء", style: .cancel) { (_) in   }
        
        
        alert.addTextField { (textField) in
            textField.placeholder = "كلمة المرور القديمة"
            textField.textAlignment = .center
            textField.isSecureTextEntry = true
        }
        
        alert.addTextField { (textField2) in
            textField2.placeholder = "كلمة المرور الجديدة"
            textField2.textAlignment = .center
            textField2.isSecureTextEntry = true
        }
        
        alert.addAction(confirmaction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
