//
//  SignUpVC.swift
//  Fresh
//
//  Created by Monica Girgis on 5/4/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire

class SignUpVC: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource , UITextFieldDelegate , UITextViewDelegate{
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var special: UITextView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var areaPicker: UIPickerView!
    @IBOutlet weak var myscrollview: UIScrollView!
    @IBOutlet weak var secview: UIView!
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var citiesName = [String]()
    var citiesID = [String]()
    var citySelectedIndex : Int = 0
    
    @IBAction func signUp(_ sender: UIButton) {
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let parameters : Parameters = [
            "username":name.text!,
            "phone":mobile.text!,
            "email":email.text!,
            "password":password.text!,
            "address":address.text!,
            "special_location":special.text!,
            "city_id":citiesID[citySelectedIndex]]
        
        Alamofire.request("\(MainVC.url)register.php",method: .post,parameters: parameters).responseString { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                if ((resultString?.range(of: "true")) != nil)
                {
                    let alert = UIAlertController(title: "تم التسجيل", message: "يمكنك الدخول الان", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: { action in
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                        self.present(newViewController, animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertController(title: "خطأ", message: "برجاء اعادة المحاولة", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        address.delegate = self
        email.delegate = self
        special.delegate = self
        password.delegate = self
        mobile.delegate = self
        
        address.text = "    العنوان بالتفصيل    "
        address.textColor = UIColor.lightGray
        
        special.text = "     اقرب علامة مميزة    "
        special.textColor = UIColor.lightGray
        
        // add tapgesture to dismiss keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.secview.addGestureRecognizer(tapGesture)
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        Alamofire.request("\(MainVC.url)cities.php").responseJSON{ response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                if let array = response.result.value as? [[String: Any]] {
                    self.citiesName = array.flatMap { $0["name"] as? String }
                    self.citiesID = array.flatMap { $0["id"] as? String }
                    self.areaPicker.reloadAllComponents()
                    print(self.citiesName)
                }
                else
                {
                    print("fail")
                }
            }
        }
        
        // Do any additional setup after loading the view.
        self.areaPicker.dataSource = self
        self.areaPicker.delegate = self
    }
    
    // Mina Add Tapgesture to dismiss keyboard when tap on view
    @IBAction func tapGestureHandler(gesture: UITapGestureRecognizer)
    {
        
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        myscrollview.setContentOffset(CGPoint.init(x: 0, y: 400), animated: true)
        
        if address.textColor == UIColor.lightGray && textView == address{
            address.text = nil
            address.textColor = UIColor.black
        }
        
        if special.textColor == UIColor.lightGray && textView == special {
            special.text = nil
            special.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        myscrollview.setContentOffset(CGPoint.init(x: 0, y: myscrollview.contentOffset.y - 100), animated: true)
        
        if address.text.isEmpty {
            address.text = "    العنوان بالتفصيل    "
            address.textColor = UIColor.lightGray
        }
        
        if special.text.isEmpty {
            special.text = "     اقرب علامة مميزة    "
            special.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        name.resignFirstResponder()
        email.resignFirstResponder()
        password.resignFirstResponder()
        mobile.resignFirstResponder()
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citiesName.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return citiesName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        citySelectedIndex = row
    }
}
