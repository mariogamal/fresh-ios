//
//  Product.swift
//  Fresh
//
//  Created by Monica Girgis on 5/5/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation

class Product {
    var proName: String!
    var proID: String!
    var proQty: Int!
    var proPrice: String!
    var proImg: String!
    var choosed: Bool!
    
    init (id: String , name :String , img :String , Qty: Int , price:String , choose:Bool)
    {
        proImg = img
        proPrice = price
        proName = name
        proID = id
        proQty = Qty
        choosed = choose
    }
}
