//
//  MainVC.swift
//  Fresh
//
//  Created by Monica Girgis on 3/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire

class MainVC: UIViewController , UITextFieldDelegate{
    
    static let url = "http://104.218.120.144/~zadmin/fresh/api/"
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var passText: UITextField!
    
    @IBAction func login(_ sender: UIButton) {
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let parameters : Parameters = [
            "email":emailText.text!,
            "password":passText.text!,
            "refresh_token":"token",
            "os_version":UIDevice.current.systemVersion,
            "device_model":modelIdentifier()]
        
        Alamofire.request("\(MainVC.url)login.php",method: .post,parameters: parameters).responseJSON { response in
            self.activityIndicator.stopAnimating()
            
            if(response.result.isSuccess)
            {
                
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                //                let result = response.data
                print(response)
                do {
                    let myuser = response.result.value as? [String: Any]
                    
                    let msg = myuser!["msg"] as? Int
                    let user_id = myuser!["user_id"] as? String
                    
                    if (msg == 1)
                    {
                        let userdefault = UserDefaults.standard
                        userdefault.set(user_id, forKey: "user_id")
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "FoodCategories") as! FoodCategories
                        self.present(newViewController, animated: true, completion: nil)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "خطأ", message: "البيانات غير صحيحة", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
    
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        passText.delegate = self
        emailText.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let userdefault = UserDefaults.standard
        if (userdefault.string(forKey: "user_id") != nil)
        {
            performSegue(withIdentifier: "login", sender: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailText.resignFirstResponder()
        passText.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}
