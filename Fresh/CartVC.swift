//
//  CartVC.swift
//  Fresh
//
//  Created by Mario Gamal on 5/10/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire

class CartVC: UIViewController ,UITableViewDelegate, UITableViewDataSource{
    
    var hasDiscount = "0"
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var totalMoney: UILabel!
    @IBOutlet weak var cartTable: UITableView!
    @IBAction func disscountBtn(_ sender: UIBarButtonItem)
    {
        let alertController = UIAlertController(title: "كود الخصم", message: "ادخل الكود قبل الشراء فقط", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = ""
        }
        let confirmAction = UIAlertAction(title: "اتمام", style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
            self.validateDiscount(code: String(describing: textField.text))
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "الغاء", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    @IBAction func placeOrder(_ sender: UIButton) {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        if (ProductsVC.cartItems.count > 0)
        {
            let userdefault = UserDefaults.standard
            if (userdefault.string(forKey: "user_id") == nil)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                self.present(newViewController, animated: true, completion: nil)
            }
            else
            {
                placeOrderRequest()
            }
        }
            
        else
        {
            let alert = UIAlertController(title: "خطأ", message: "برجاء اضافة منتجات اولا", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartTable.allowsSelection = false
        cartTable.delegate = self
        cartTable.dataSource = self
        calcTotalMoney()
    }
    
    func validateDiscount(code : String){
        let parameters : Parameters = ["code":code]
        Alamofire.request("\(MainVC.url)validate_coupon.php",method: .post,parameters: parameters).responseString { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                if ((resultString?.range(of: "true")) != nil)
                {
                    self.hasDiscount = "1"
                    let alert = UIAlertController(title: "كود الخصم", message: "تم اضافة الخصم", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.hasDiscount = "0"
                    let alert = UIAlertController(title: "كود الخصم", message: "الكود غير صحيح", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func calcTotalMoney(){
        var totalmoney : Float = 0.0
        for product in ProductsVC.cartItems{
            totalmoney += (Float(product.proQty) * Float(product.proPrice)!)
        }
        totalMoney.text = "اجمالى الفاتورة ="+"\(totalmoney) "+"جنية "
    }
    
    func calcDisccount(){
        var totalmoney : Float = 0.0
        for product in ProductsVC.cartItems{
            totalmoney += (Float(product.proQty) * Float(product.proPrice)!)
        }
        totalmoney = (totalmoney*90)/100
        totalMoney.text = "اجمالى الفاتورة ="+"\(totalmoney) "+"جنية "
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductsVC.cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cartTable.dequeueReusableCell(withIdentifier: "productCell") as! productCell
        cell.proName.text = ProductsVC.cartItems[indexPath.row].proName
        cell.proPrice.text = ProductsVC.cartItems[indexPath.row].proPrice
        let url = URL(string: ProductsVC.cartItems[indexPath.row].proImg)!
        cell.proImage.stx.image(atURL: url)
        cell.proQty.text = String(ProductsVC.cartItems[indexPath.row].proQty)
        cell.buyProduct.tag = indexPath.row
        cell.buyProduct.addTarget(self, action: #selector(removeItem(sender:)), for: .touchUpInside)
        cell.increaseQty.tag = indexPath.row
        cell.increaseQty.addTarget(self, action:#selector(increaseQty(sender:)), for: .touchUpInside)
        cell.decreaseQty.tag = indexPath.row
        cell.decreaseQty.addTarget(self, action:#selector(decreaseQty(sender:)), for: .touchUpInside)
        return cell
    }
    
    func removeItem(sender: UIButton){
        let index = sender.tag
        ProductsVC.cartItems.remove(at: index)
        cartTable.reloadData()
        calcTotalMoney()
    }
    
    func increaseQty(sender: UIButton){
        let index = sender.tag
        ProductsVC.cartItems[index].proQty = (ProductsVC.cartItems[index].proQty)+1
        cartTable.reloadData()
        calcTotalMoney()
    }
    
    func decreaseQty(sender: UIButton){
        let index = sender.tag
        if (ProductsVC.cartItems[index].proQty>1)
        {
            ProductsVC.cartItems[index].proQty = (ProductsVC.cartItems[index].proQty)-1
            cartTable.reloadData()
        }
        calcTotalMoney()
    }
    
    func prepareJson() -> String {
        let jsonArray:NSMutableArray = NSMutableArray()
        
        for product in ProductsVC.cartItems
        {
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            jsonObject.setValue(product.proName, forKey: "name")
            jsonObject.setValue(product.proQty, forKey: "qty")
            jsonObject.setValue(product.proPrice, forKey: "price")
            jsonArray.add(jsonObject)
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: jsonArray)
            let dataString = String(data: data, encoding: .utf8)!
            print(dataString)
            return dataString
        } catch {
            print("JSON serialization failed: ", error)
            return ""
        }
    }
    
    func placeOrderRequest (){
        let userdefault = UserDefaults.standard
        let parameters : Parameters = [
            "user_id":"\(userdefault.string(forKey: "user_id")!)",
            "confirmation_status":"waiting",
            "order_items":prepareJson(),
            "has_discount": hasDiscount]
        
        print(parameters)
        
        Alamofire.request("\(MainVC.url)place_order.php",method: .post,parameters: parameters).responseString { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                if ((resultString?.range(of: "true")) != nil)
                {
                    ProductsVC.cartItems.removeAll()
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "OrdersVC") as! OrdersVC
                    self.present(newViewController, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertController(title: "خطأ", message: "البيانات غير صحيحة", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
