//
//  ProductsVC.swift
//  Fresh
//
//  Created by Monica Girgis on 4/1/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire
import STXImageCache

class ProductsVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    
    @IBAction func openProfile(_ sender: Any) {
        
        let userdefault = UserDefaults.standard
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if (userdefault.string(forKey: "user_id") == nil)
        {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
            self.present(newViewController, animated: true, completion: nil)
        }
        else
        {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
            self.present(newViewController, animated: true, completion: nil)
            
        }
        
    }
    @IBOutlet weak var cartBarBtn: UIBarButtonItem!
    @IBOutlet weak var productSearch: UISearchBar!
    @IBOutlet weak var productsTable: UITableView!
    var products:[Product] = []
    var productsFull:[Product] = []
    static var cartItems:[Product] = []
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productsTable.delegate = self
        productsTable.dataSource = self
        productSearch.delegate = self
        
        let parameter : Parameters = ["category_id":FoodCategories.categoryID+1]
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        Alamofire.request("\(MainVC.url)category_products.php",method: .post,parameters: parameter).responseJSON{ response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                if let jsonArray = response.result.value as? [[String: Any]] {
                    print(jsonArray)
                    for jsonObject in jsonArray
                    {
                        guard
                            let id = jsonObject["id"] as? String,
                            let name = jsonObject["name"] as? String,
                            let price = jsonObject["item_price"] as? String,
                            let image = jsonObject["imag_url"] as? String
                            else {continue}
                        self.productsFull.append(Product(id: id,name: name,img: image,Qty:1,price: price, choose: false))
                        print(id , name)
                    }
                    self.disableCartItems()
                    self.products = self.productsFull
                    self.productsTable.reloadData()
                }
                else
                {
                    print("fail")
                }
            }
        }
        //------------------- bar button badge
        // badge label
        let label = UILabel(frame: CGRect(x: 10, y: -10, width: 20, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "SanFranciscoText-Light", size: 13)
        label.textColor = .white
        label.backgroundColor = .red
        label.text = "80"
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage(UIImage(named: "ic_cart"), for: .normal)
        rightButton.addTarget(self, action: #selector(rightButtonTouched), for: .touchUpInside)
        rightButton.addSubview(label)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        
        navigationItem.rightBarButtonItem = rightBarButtomItem
        
    }
    
    func rightButtonTouched() {
        print("right button touched")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableCartItems (){
        if (ProductsVC.cartItems.count > 0)
        {
            for product in productsFull{
                if (itemFound(id: product.proID) == true){
                    product.choosed = true
                }
            }
        }
    }
    
    func itemFound (id : String)-> Bool{
        for product in ProductsVC.cartItems{
            if(product.proID == id){
                return true
            }
        }
        return false
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = productsTable.dequeueReusableCell(withIdentifier: "productCell") as! productCell
        cell.selectionStyle = .none
        cell.proName.text = products[indexPath.row].proName
        cell.proPrice.text = products[indexPath.row].proPrice
        let url = URL(string: products[indexPath.row].proImg)!
        cell.proImage.stx.image(atURL: url)
        cell.proQty.text = String(products[indexPath.row].proQty)
        cell.increaseQty.tag = indexPath.row
        cell.increaseQty.addTarget(self, action:#selector(increaseQty(sender:)), for: .touchUpInside)
        cell.decreaseQty.tag = indexPath.row
        cell.decreaseQty.addTarget(self, action:#selector(decreaseQty(sender:)), for: .touchUpInside)
        if (products[indexPath.row].choosed == true)
        {
            cell.buyProduct.backgroundColor = UIColor (red: 255/255, green: 240/255, blue: 128/255, alpha: 1)
            cell.buyProduct.removeTarget(self, action: #selector(addToCart(sender:)), for: .touchUpInside)
        }
        else
        {
            cell.buyProduct.backgroundColor = UIColor (red: 255/255, green: 192/255, blue: 0/255, alpha: 1)
            cell.buyProduct.tag = indexPath.row
            cell.buyProduct.addTarget(self, action: #selector(addToCart(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    func increaseQty(sender: UIButton){
        let index = sender.tag
        products[index].proQty = (products[index].proQty)+1
        productsTable.reloadData()
    }
    
    func decreaseQty(sender: UIButton){
        let index = sender.tag
        if (products[index].proQty>1)
        {
            products[index].proQty = (products[index].proQty)-1
            productsTable.reloadData()
        }
    }
    
    func addToCart(sender: UIButton){
        ProductsVC.cartItems.append(products[sender.tag])
        products[sender.tag].choosed = true
        productsTable.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        products = searchText.isEmpty ? productsFull : productsFull.filter {
            $0.proName.range(of: searchText, options: .caseInsensitive) != nil
        }
        productsTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //self.productSearch.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        productSearch.text = ""
        productSearch.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        productSearch.showsCancelButton = false
        productSearch.text = ""
        productSearch.resignFirstResponder()
    }
    
    @IBAction func callbtn(_ sender: Any) {
        showCallDialog()
    }
    
    func showCallDialog(){
        let actionSheetController: UIAlertController = UIAlertController(title: "الخط الساخن", message: "٠١٢١١١٨٤٥٤", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "الغاء", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        
        let callAction: UIAlertAction = UIAlertAction(title: "اتصال", style: .default) { action -> Void in
            if let url = URL(string: "tel://01211118454"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        actionSheetController.addAction(callAction)
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
}
