//
//  FoodCategories.swift
//  Fresh
//
//  Created by Monica Girgis on 4/1/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class FoodCategories: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var categoriesCV: UICollectionView!
    
    @IBAction func callHotLine(_ sender: Any) {
        showCallDialog()
    }
    
    @IBAction func openProfile(_ sender: Any) {
        let userdefault = UserDefaults.standard
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        if (userdefault.string(forKey: "user_id") == nil)
        {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
            self.present(newViewController, animated: true, completion: nil)
        }
        else
        {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
            self.present(newViewController, animated: true, completion: nil)
            
        }
    }
    
    static var categoryID = -1
    var imageList = [UIImage(named: "vegatabls"),UIImage(named: "fruits"),UIImage(named: "meat"),UIImage(named: "fish"),UIImage(named: "fullbirds"),UIImage(named: "chicken"),UIImage(named: "offers"),UIImage(named: "juice"),UIImage(named: "etara"),UIImage(named: "ready"),UIImage(named: "healthy")]
    var nameList = ["خضار","فاكه","لحوم","اسماك","طيور","دواجن","عروض فريش","عصائر فريش","عطارة","خضروات جاهزة","وجبات صحية"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoriesCV.delegate = self
        categoriesCV.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCV.dequeueReusableCell(withReuseIdentifier: "catCell", for: indexPath)as! categoryCell
        cell.image.image = self.imageList[indexPath.row]
        cell.name.text = self.nameList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numOfCells : CGFloat = 2.0
        let paddingSpace : CGFloat = 3.0 * (numOfCells+1.0)
        let availableWidth = self.categoriesCV.frame.width - paddingSpace * 2.0
        let widthPerItem = availableWidth/2
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((segue.destination as? ProductsVC) != nil)
        {
            FoodCategories.categoryID = (categoriesCV.indexPathsForSelectedItems?[0].row)!
        }
    }
    
    func showCallDialog(){
        let actionSheetController: UIAlertController = UIAlertController(title: "الخط الساخن", message: "٠١٢١١١٨٤٥٤", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "الغاء", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        
        let callAction: UIAlertAction = UIAlertAction(title: "اتصال", style: .default) { action -> Void in
            if let url = URL(string: "tel://01211118454"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        actionSheetController.addAction(callAction)
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
