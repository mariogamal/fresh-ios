//
//  Order.swift
//  Fresh
//
//  Created by Mario Gamal on 5/11/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation

class Order {
    var orderID : String!
    var orderItems : [[String: Any]]!
    var orderDate : String!
    var arrivalTime : String!
    var orderState : String!
    var orderDisscount : String!
    
    init (id:String, items:[[String: Any]], date:String, arrive:String, state:String, disscount:String){
        orderID = id
        orderItems = items
        orderDate = date
        arrivalTime = arrive
        orderState = state
        orderDisscount = disscount
    }
}
