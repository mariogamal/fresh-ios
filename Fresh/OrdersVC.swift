//
//  OrdersVC.swift
//  Fresh
//
//  Created by Mario Gamal on 5/11/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import Alamofire

class OrdersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var review: String?
    var rate: String?
    var orders:[Order] = []
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var orderTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderTable.delegate = self
        orderTable.dataSource = self
        orderTable.estimatedRowHeight = 150
        orderTable.rowHeight = UITableViewAutomaticDimension
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let userdefault = UserDefaults.standard
        
        let parameters : Parameters = [
            "user_id":"\(userdefault.string(forKey: "user_id")!)",
            "confirmation_status":"1"]
        
        Alamofire.request("\(MainVC.url)track_user_orders.php",method: .post,parameters: parameters).responseJSON { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                if let jsonArray = response.result.value as? [[String: Any]] {
                    print(jsonArray)
                    for jsonObject in jsonArray
                    {
                        guard
                            let id = jsonObject["id"] as? String,
                            let state = jsonObject["confirmation_status"] as? String,
                            let items = jsonObject["order_items"] as? String,
                            let date = jsonObject["created_on"] as? String,
                            let arrive = jsonObject["delivery_time"] as? String,
                            let disscount = jsonObject["has_discount"] as? String
                            else {
                                print("fail")
                                continue
                        }
                        let data = items.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                        do {
                            let jsonItems = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: AnyObject]]
                            self.orders.append(Order(id:id, items:jsonItems, date:date, arrive:arrive, state:state, disscount:disscount))
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                    self.orderTable.reloadData()
                }
                else
                {
                    print("fail")
                }
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return orders.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTable.dequeueReusableCell(withIdentifier: "orderCell") as! OrderCell
        cell.selectionStyle = .none
        cell.orderDate.text = orders[indexPath.section].orderDate
        cell.arriveTime.text = orders[indexPath.section].arrivalTime
        cell.actionBtn.tag = Int(orders[indexPath.section].orderID)!
        if (orders[indexPath.section].orderState == "accepted")
        {
            cell.actionBtn.setTitle("استلمت", for: .normal)
            cell.actionBtn.backgroundColor = UIColor.green
            cell.actionBtn.addTarget(self, action:#selector(confirmOrder(sender:)), for: .touchUpInside)
            cell.orderState.text = "تم الرد على طلبك"
        }
        else
        {
            cell.actionBtn.addTarget(self, action:#selector(cancelOrder(sender:)), for: .touchUpInside)
            cell.orderState.text = "قيد الانتظار"
        }
        let jsonArray = orders[indexPath.section].orderItems
        var itemNames = ""
        var itemsQty = ""
        var itemsPrice = ""
        var total = 0.0
        for jsonObject in jsonArray! {
            guard
                let name = jsonObject["name"] as? String,
                let price = jsonObject["price"] as? String,
                let qty = jsonObject["qty"] as? Int
                else {
                    print("error")
                    continue
                    
            }
            total += Double(qty)*Double(price)!
            itemNames += name+"\n"
            itemsQty += "\(qty)"+"\n"
            itemsPrice += price+"\n"
        }
        
        cell.proName.text = itemNames
        cell.proPrice.text = itemsPrice
        cell.proQty.text = itemsQty
        cell.proName.sizeToFit()
        cell.proPrice.sizeToFit()
        cell.proQty.sizeToFit()
        if (orders[indexPath.section].orderDisscount=="0"){
            cell.totalMoney.text = String(total)
        }
        else {
             cell.totalMoney.text = String((total*90)/100)
        }
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        return cell
    }

    func confirmOrder(sender: UIButton){
        
        let alert = UIAlertController(title: "تقييم الطلب",
                                      message: "",
                                      preferredStyle: .alert)
        
        let confirmaction = UIAlertAction(title: "حسنا",style: .default) { (_) in
            
            guard let textFields = alert.textFields,
                textFields.count > 0 else {
                    return
            }
            
            self.review = textFields[0].text
            self.rate = textFields[1].text
            
            let userdefault = UserDefaults.standard
            let parameters : Parameters = [
                "user_id":"\(userdefault.string(forKey: "user_id")!)",
                "rate":self.rate!,
                "review":self.review!,
                "order_id":sender.tag]
            
            Alamofire.request("\(MainVC.url)confirm_order.php",method: .post,parameters: parameters).responseString { response in
                self.activityIndicator.stopAnimating()
                if(response.result.isSuccess)
                {
                    let resultString = response.result.value
                    if ((resultString?.range(of: "true")) != nil)
                    {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
                        self.present(newViewController, animated: true, completion: nil)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "خطأ", message: "تعذر الاتصال", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "الغاء", style: .cancel) { (_) in   }
        
        
        alert.addTextField { (textField) in
            textField.placeholder = "اكتب تعليقك"
            textField.textAlignment = .center
        }
        
        alert.addTextField { (textField2) in
            textField2.placeholder = "التقييم من ١ الى ٥"
            textField2.textAlignment = .center
        }
        
        alert.addAction(confirmaction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelOrder(sender: UIButton){
        
        let userdefault = UserDefaults.standard
        let parameters : Parameters = [
            "user_id":"\(userdefault.string(forKey: "user_id")!)",
            "order_id":sender.tag]
        
        Alamofire.request("\(MainVC.url)cancle_order.php",method: .post,parameters: parameters).responseString { response in
            self.activityIndicator.stopAnimating()
            if(response.result.isSuccess)
            {
                let resultString = response.result.value
                print("Response String: \(resultString!)")
                if ((resultString?.range(of: "true")) != nil)
                {
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
                    self.present(newViewController, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertController(title: "خطأ", message: "تعذر الاتصال", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "متابعة", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
